// abr.h

#include <stdio.h>
#include <math.h>

/*
 * Un ABR = un pointeur de type (struct abr*)
 * Les 'struct abr' sont alloués dynamiquement (dans le tas)
 * L'ABR vide = NIL (c'est bien un pointeur)
 */

struct abr {
    struct abr* gauche; // toutes les valeurs à gauche sont < valeur
    int valeur;      // la valeur contenue dans ce noeud-ci
    struct abr* droit;  // toutes les valeurs à droite sont > valeur
};

#define NIL (struct abr*)0

/*
 * Pas besoin de constructeur. Il suffit d'initialiser à NIL
 */

// Destructeur
extern void destruct_abr (struct abr* A);

/*
 * La fonction prend en paramètre un ABR A et une valeur val.
 * Elle retourne l'ABR obtenu après ajout de val dans A.
 */

extern struct abr* ajout_abr (int val, struct abr* A);

/*
 * Retourne true si val appartient à A, false sinon.
 */

#include <stdbool.h>

/*
 * Retourne true si cle appartient à A, false sinon.
 * Les deux paramètres en mode donnée
 */

extern bool recherche_abr (int cle, struct abr* A);

extern void print_abr (struct abr* A);

extern void print_dot(struct abr* A);

extern void print_dot_aux(FILE* f,struct abr* A);

extern int hauteur_abr(struct abr* A);

extern int nb_noeuds_abr(struct abr* A);

