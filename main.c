// main.c

#include "abr.h"
#include <stdio.h>
#include <math.h>


int main ()
{
    struct abr* racine;
    int x;
    racine = NIL;
    scanf ("%d", &x);
    
    while (x != -1)
    {
        racine = ajout_abr (x, racine);
        
        scanf ("%d", &x);
    }
    
    print_abr(racine);

    printf ("la hauteur de l'ABR est %d\n", hauteur_abr(racine));
    printf ("le nombre de noeuds de l'ABR est %d\n",nb_noeuds_abr(racine));
    
    print_dot(racine);
    
	destruct_abr (racine);
	return 0;
}
