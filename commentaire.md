14 avril
--------

Bonjour Emma,

Concernant le dépôt git, il ne faut faire superviser par git
que les fichiers source. Tu devrais donc retirer les fichier '.o' et '.out'

git rm [les fichiers à ne plus superviser]
git commit
git push

Il y a un petit bug d'affichage (étiquette 'droit' sur le fichier .dot).

Essaie d'indenter ton code de façon plus homogène (indentation de 4 espaces
par exemple).

Sinon c'est bien

23 avril
--------

Warning à la compilation :

abr.c: In function ‘print_dot’:
abr.c:142:21: warning: format ‘%f’ expects argument of type ‘double’, but argument 3 has type ‘int’ [-Wformat=]
         fprintf(f,"%f ; \n", A->valeur);
                    ~^        ~~~~~~~~~
                    %d

hauteur_abr n'est pas très bien indenté
