//abr.c
#include "abr.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>


// Destructeur 

void destruct_abr(struct abr * A)
{
        if(A!= NIL)
        {
            destruct_abr(A->gauche);
            destruct_abr(A->droit);
            free(A);
        }
}


// Afficher 

void print_abr( struct abr * A)
{
    if (A != NIL)
    {
        print_abr(A->gauche);
        printf(" %d ", A->valeur);
        print_abr(A->droit);
	       
    }
    
}

// NB noeuds
int nbNoeuds( struct abr * A)
{
    if(A== NIL) return 0;
    else 
    {
        return 1 + nbNoeuds(A->gauche) + nbNoeuds(A->droit);
    }
}

// AJOUT 

/*
 * Fabrique une feuille de valeur val et la retourne
 */

static struct abr* new_feuille (int val)
{   struct abr* F;
    F = (struct abr*)malloc (sizeof (struct abr));
    F->gauche = NIL;
    F->valeur = val;
    F->droit = NIL;
    return F;
}

/*
 * La fonction  ajout_abr() prend en paramètre un ABR A0 ainsi qu'une valeur val.
 * Elle retourne l'ABR obtenu après ajout de val dans A0.
 */

struct abr* ajout_abr (int val, struct abr* A0)
{
    if (A0 == NIL) return new_feuille (val);
    else // A ce niveau on sait que l'arbre n'est pas vide puisqu'on l'a vérifié dans le if
    {   struct abr* A;
        struct abr* B;
        B = NIL; 
        A = A0;
        
        while (A != NIL)
        {   assert (A->valeur != val); // Erreur si val déjà présent
            B = A; // B = l'ancienne valeur de A (B suit A)
            if (A->valeur < val)
                A = A->droit;
            else
                A = A->gauche;
        }
        // Ici,  A qui vaut NIL et B qui pointe sur le noeud à modifier
        if (B->valeur < val)
            B->droit = new_feuille (val);
        else
            B->gauche = new_feuille (val);
        return A0;
    }
}


//  Cette fonction : Déterminer s'il s'agit d'une feuille 
bool est_feuille(struct abr *A) {
        bool fe;
	fe=false;
       assert(A != NIL);
       if ((A->gauche == NIL) && (A->droit == NIL) ) fe=true;
       return fe;
}



// Doit afficher les lignes du fichier '.dot' p.e. 15 -> 46 [label="droit"];
// Fonction auxiliaire récursive
// Les affichages doivent se faire dans f

// On peut supposer que A est différent de NIL


 void print_dot_aux ( FILE* f, struct abr* A)
{
    if(A->gauche !=NIL)
    {
        fprintf(f," %d -> %d [label=\"gauche\"]; \n",A->valeur,A->gauche->valeur);
        print_dot_aux(f,A->gauche);
    }
    
    if(A->droit !=NIL)
    {
        fprintf(f," %d -> %d  [label=\"droit\"];\n ",A->valeur,A->droit->valeur);
        print_dot_aux(f,A->droit);
    }
}


// Fonction principale

void print_dot (struct abr* A)
{   
    FILE* f;

    f = fopen ("ABR.dot", "w");
    assert (f != NULL);
    // 1ère ligne de ABR.dot
    fprintf (f, "digraph G {\n");

    if (A == NIL) fprintf (f, " NIL;\n");
    else if (est_feuille (A))
        // Affiche la valeur de A suivie d'un ';' dans f car ici c'est une feuille on a donc des sous arbre vide à gauche et à droite
        fprintf(f,"%f ; \n", A->valeur);
        else print_dot_aux (f,A); // On est sûr que A != NIL

    fprintf (f, "}\n");
    fclose (f);
    
    system ("dot -Tpdf ABR.dot -Grankdir=LR -o ABR.pdf");
    //system ("evince ABR.pdf");
}


// Calculer la hauteur de l'abr

int max(int a, int b)
{
    int max;
    max=0;
    
    if(a<b) max=b;
    else max=a;
    
    return max;
    
}


int hauteur_abr(struct abr * A)
{
	 if (A == NIL) return -1;
     else
     {
        if(A->gauche==NIL && A->droit==NIL) return 0;
        else{ printf("\n") ;return 1+ max(hauteur_abr(A->gauche),hauteur_abr(A->droit));}
     }

}

int nb_noeuds_abr(struct abr * A)
{
     if(A==NIL) return 0;
     else return 1+ nb_noeuds_abr(A->gauche) + nb_noeuds_abr(A->droit);
    
}
